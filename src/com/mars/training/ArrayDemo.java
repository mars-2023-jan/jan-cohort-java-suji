package com.mars.training;

import java.util.Scanner;

public class ArrayDemo {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		String[] str = sc.nextLine().split("");

//			System.out.print(str[i]);

		int n = str.length;
		String[] repeatWords = new String[n];
		for (int i = 0; i < n; i++) {
			if (contains(repeatWords, str[i])) {
				System.out.println("The first repeating string in \"" + str + "\" is: \"" + str[i] + "\"");
				break;
			}
			repeatWords[i] = str[i];
		}
		if (!contains(repeatWords, null)) {
			System.out.println("There is no repeating string in \"" + str + "\".");
		}
	}

	public static boolean contains(String[] arr, String target) {
		for (String s : arr) {
			if (s == null) {
				return false;
			}
			if (s.equals(target)) {
				return true;
			}
		}
		return false;
	}
}

//		int[] numbers = new int[5]; // size 5 without any values. //one-dimensional array
//
//		int[][] y = new int[3][]; // length 3 //two dimensional array.
//
//		y[0] = new int[2]; // 2columns
//		y[1] = new int[3];
//		y[2] = new int[2];
//
//		for (int i = 0; i < y.length; i++) {
//			for (int j = 0; j < y[i].length; j++) {
//				y[i][j] = i * 2;
//				System.out.println(y[i][j]);
//			}
//			System.out.println();
//		}

//		Scanner sc = new Scanner(System.in);
//
//		System.out.println("Enter 5 numbers for the array:");
//
//		for (int i = 0; i < numbers.length; i++) {
//			int num = sc.nextInt();
//			numbers[i] = num;
//
//		}
//		System.out.println("Entered array is:");
//		for (int i = 0; i < numbers.length; i++) {
//			System.out.println(numbers[i]);

//		numbers[0] = 12;
//		numbers[4] = 20;
//
////		int[] newNum = {10,20,30}; //size 3
//
//		System.out.println(numbers[1]); // 0
//		System.out.println(numbers[0]); // 12
//		System.out.println(numbers[4]); // 20
//		System.out.println(numbers[5]); // since the index is not there it will throw an exception

//Learning Java is fun java has lot of things

//Create a class name Shop with attributes: shopname, shop address, products(String array)
//create a parameterized constructors for shop
//Create a checkProducutavailability method which will take a product name a parameters.
//create main method which will take inputs from the user using scanner class and search for the productname 

//main
//shop ob = new shop(name, address,products)

//while loop
//q then quit and go for search
