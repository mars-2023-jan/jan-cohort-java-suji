package com.mars.training;

import java.util.Scanner;

public class DataDemo {

	public static void main(String[] args) {

		// Local Variables

		byte b = 25;
		int i = b; // Implicit type cast
		short s = (short) i; // Explicit type cast
		float f = 30.8f;

		Scanner sc = new Scanner(System.in);
		System.out.println("Input first number : ");

		int firstNumber = sc.nextInt();
		System.out.println("Input second number : ");

		int secondNumber = sc.nextInt();

		DataDemo demo1 = new DataDemo();

		int sum = add(firstNumber, secondNumber);

		System.out.println("Sum of two numbers is:" + sum);

		System.out.println("Enter your Name : ");

		String name = sc.nextLine();

		System.out.println("Name is " + name);
	}

	private static int add(int a, int b) {
		return a + b;
	}
}
