package com.mars.training;

public class Employee {

	// Instance Variables
	private String empName;
	private String empAddress;
	private double salary;
	private int empId;
	private String designation;

	// Static Variable
	static String companyName;

	public Employee() {
		super();
	}

	public Employee(int empId, String empName, String empAddress, double salary, String designation) {
		super();
		this.empName = empName;
		this.empAddress = empAddress;
		this.salary = salary;
		this.empId = empId;
		this.designation = designation;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpAddress() {
		return empAddress;
	}

	public void setEmpAddress(String empAddress) {
		this.empAddress = empAddress;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	@Override
	public String toString() {
		return "Employee [empName=" + empName + ", empAddress=" + empAddress + ", salary=" + salary + ", empId=" + empId
				+ ", designation=" + designation + "]";
	}

}
