package com.mars.training;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.mars.training.exception.MyException;

public class ExceptionHandling {

	public static void main(String[] args) {

		try {
			FileReader file = new FileReader("test.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int x = 2;
		try {
			int y = 6 / x;
			int z = 67;

			if (y > 2) {
				throw new MyException("MyException thrown");
			}

		} catch (ArithmeticException | NumberFormatException | MyException e) {
			if (e instanceof MyException) {
				System.out.println(((Throwable) e).getMessage());
			} else
				System.out.println("Arithmetic Exception Occured...");
		} finally {
			System.out.println("Finally Block");
		}
		System.out.println("Program Continues...");
	}

}

//1. Checked Exception - Compiled time exceptions are checked exception
//    eg : File not found, ToException, SQLException,Interrupted Exception
//2. unchecked Exception  - Thrown only during Runtime
//    eg : ArithmeticExceptoin,ArrayIndexoutofBoundException, NumberFormatException