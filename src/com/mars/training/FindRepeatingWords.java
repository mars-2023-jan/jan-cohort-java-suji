package com.mars.training;

import java.util.Scanner;

public class FindRepeatingWords {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the sentence : ");
		String sentence = scanner.nextLine();

		String[] words = sentence.split(" ");

		String firstRepeatingWord = "";
		int countFirstRepeatingWord = 1;

		for (int i = 0; i < words.length; i++) {

			for (int j = i + 1; j < words.length; j++) {
				if (words[i].equals(words[j])) {
					firstRepeatingWord = words[i];
					countFirstRepeatingWord++;
				}
			}
			if (firstRepeatingWord != "") {
				break;
			}
		}

		System.out.println("First Repeating Word is " + firstRepeatingWord + " , and repeated "
				+ countFirstRepeatingWord + " times");

	}

}
