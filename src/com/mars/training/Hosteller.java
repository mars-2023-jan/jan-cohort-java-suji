package com.mars.training;

public class Hosteller extends Student {

	private int roomNumber;
	private String blockName;

	public Hosteller() {
		super();

	}

	public Hosteller(String fullName, String id, String department, String phoneNumber, int roomNumber,
			String blockName) {
		super(fullName, id, department, phoneNumber);
		this.roomNumber = roomNumber;
		this.blockName = blockName;

	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getBlockName() {
		return blockName;
	}

	public void setBlockName(String blockName) {
		this.blockName = blockName;
	}

	@Override
	public void updatePhoneNumber(String phoneNumber) {
		super.setPhoneNumber(phoneNumber);

	}

	@Override
	public String toString() {
		return "Hosteller [roomNumber=" + roomNumber + ", blockName=" + blockName + ", getFullName()=" + getFullName()
				+ ", getId()=" + getId() + ", getDepartment()=" + getDepartment() + ", getPhoneNumber()="
				+ getPhoneNumber() + "]";
	}

	@Override
	public void updateRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;

	}

}
