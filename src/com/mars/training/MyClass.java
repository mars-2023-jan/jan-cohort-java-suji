package com.mars.training;

public class MyClass {

	public static void main(String[] args) {
		System.out.println("Main Method");

		String myString1 = "Hello";
		String myString2 = "Hello";// String Literal

		myString1.concat("World");
		System.out.println(myString1); // Strings are immutable(Output = hello) i we reassign we can change the value.

		StringBuffer sb = new StringBuffer("abc"); // StringBuffer is mutable.
		sb.append("Technologies");
		System.out.println(sb);

		myString1 = "Hello You";

		String str1 = new String("World"); // String Object

		String str2 = new String("World");

		System.out.println(str1.equals(str2)); // compare the contents in the object

		System.out.println(str1 == str2);

		System.out.println(myString1 == myString2);

	}

	// Static Block
	static {
		System.out.println("Static Block");
	}
	static {
		System.out.println("Second static Block");
	}

}
