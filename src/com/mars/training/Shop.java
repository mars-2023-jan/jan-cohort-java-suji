package com.mars.training;

import java.util.Arrays;

public class Shop {

	private String shopName;
	private String shopaddr;
	private String[] products;

	public Shop(String shopName, String shopaddr, String[] products) {
		this.shopName = shopName;
		this.shopaddr = shopaddr;
		this.products = products;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopaddr() {
		return shopaddr;
	}

	public void setShopaddr(String shopaddr) {
		this.shopaddr = shopaddr;
	}

	public String[] getProducts() {
		return products;
	}

	public void setProducts(String[] products) {
		this.products = products;
	}

	public boolean checkProductAvailability(String productName) {
		boolean exists = false;
		for (int i = 0; i < products.length; i++) {
			if (products[i].equals(productName)) {
				exists = true;
				break;
			}
		}
		return exists;
	}

	@Override
	public String toString() {
		return "Shop [shopName=" + shopName + ", shopaddr=" + shopaddr + ", products=" + Arrays.toString(products)
				+ "]";
	}

}
