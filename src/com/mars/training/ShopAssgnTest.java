package com.mars.training;

import java.util.Scanner;

public class ShopAssgnTest {

	public static void main(String[] args) {

		String[] products = new String[] { "ipad", "blanket", "rugs" };

		Shop shop = new Shop("Walmart", "GreenBay", products);

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the product name to search : ");
		String input = scanner.nextLine();

		if (shop.checkProductAvailability(input)) {
			System.out.println("Great, Found the product " + input + " at " + shop.getShopName());
		} else {
			System.out.println("Oops, this product " + input + " does NOT exists at " + shop.getShopName());
		}

	}
}
