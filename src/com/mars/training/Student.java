package com.mars.training;

public abstract class Student {

	private String fullName;
	private String id;
	private String department;
	private String phoneNumber;

	public Student() {
		super();
	}

	public Student(String fullName, String id, String department, String phoneNumber) {
		super();
		this.fullName = fullName;
		this.id = id;
		this.department = department;
		this.phoneNumber = phoneNumber;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public abstract void updatePhoneNumber(String phoneNumber);

	public abstract void updateRoomNumber(int roomNumber);
}
