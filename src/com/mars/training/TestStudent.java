package com.mars.training;

import java.util.Scanner;

public class TestStudent {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		Student s1 = new Hosteller("David Miller", "S101", "IT", "676-567-6543", 1001, "East Block");
		// Student s2 = new Hosteller("Ann Mary", "S102", "ECE", "776-597-8943", 1002,
		// "North Block");

		System.out.println("Enter 1 to change Room Number or 2 to change Phone Number: ");
		int choice = sc.nextInt();

		if (choice == 1) {
			System.out.println("Enter Room Number : ");
			int roomNumber = sc.nextInt();
			s1.updateRoomNumber(roomNumber);
		} else if (choice == 2) {
			System.out.println("Enter Phone Number:");
			String phoneNumber = sc.next();
			s1.updatePhoneNumber(phoneNumber);

		} else {
			System.out.println("Invalid Entry. Enter 1 or 2 : ");
		}

		System.out.println(s1.toString());

	}

}

//Design an application you will have a student class and a hostler class(child), in stu diff fields dep id, stu id, name , phone no
//hostler hosteller name,--- // create a input from user and create an object (student s1 = new Hostller)
//app allow to update room no of student and phone no(ask user room or ph)
//display the student details 
//Factory design pattern