package com.mars.training.abstraction;

public abstract class Car {

	private CarType model;

	// constructor overloading
	public Car() {

	}

	public CarType getModel() {
		return model;
	}

	public void setModel(CarType model) {
		this.model = model;
	}

	public Car(CarType model) {
		this.model = model;
	}

	protected abstract void construct();

	public String getDetails() {
		return "Car Details";
	}

	public String getDetails(String name) {
		return name;
	}
}