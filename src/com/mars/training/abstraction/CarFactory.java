package com.mars.training.abstraction;

public class CarFactory {
	public static Car buildCar(CarType model) {
		Car car = null; // car object
		switch (model) {
		case SMALL:
			car = new SmallCar();
			break;

		case SEDAN:
			car = new SedanCar();
			break;

		case LUXURY:
			car = new LuxuryCar();
			break;

		default:
			System.out.println("Invalid Cartype");
		}

		return car;
	}
}
