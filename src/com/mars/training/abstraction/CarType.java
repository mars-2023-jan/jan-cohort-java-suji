package com.mars.training.abstraction;

public enum CarType {

	SMALL, SEDAN, LUXURY

}
