package com.mars.training.abstraction;

public abstract class Shape { // for the class to be abstract we need at least 1 abstract method

	public abstract double area(); // Method Definition (no implementation), so change to abstract

}
