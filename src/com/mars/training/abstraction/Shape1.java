package com.mars.training.abstraction;

public interface Shape1 {

	double area();

}

//Interface is 100% abstract class and all the methods by default is public (bcz they need to be implemented)