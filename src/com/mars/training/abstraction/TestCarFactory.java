package com.mars.training.abstraction;

import java.util.Scanner;

public class TestCarFactory {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the type of car to be build: ");

		String carType = sc.next().toUpperCase();

		CarFactory.buildCar(CarType.valueOf(carType)); // string from user and converting it to enum

		Car c1 = new SmallCar();

		// super class reference can refer to subclass object

//		System.out.println(c1.getDetails());

		// Static Polymorphism - Method Overloading
		System.out.println(c1.getDetails("New Car"));

		// Dynamic Polymorphism - Method Overriding

	}

}

//Design an application you will have a student class and a hostler class(child), in stu diff fields dep id, stu id, name , phone no
// hostler hosteller name,--- // create a input from user and create an object (student s1 = new Hostler)
//app allow to update room no of student and phone no(ask user room or ph)
//display the student details 
//Factory design pattern