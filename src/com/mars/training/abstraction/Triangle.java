package com.mars.training.abstraction;

public class Triangle extends Shape implements Shape1 { // Like a multiple inheritance can implement interface

	private double base;
	private double height;

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public double area() {

		return 0.5 * base * height;
	}

}
