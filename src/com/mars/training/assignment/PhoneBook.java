package com.mars.training.assignment;

import java.util.HashSet;
import java.util.Set;

public class PhoneBook {
	private Set<Contact> contactList = new HashSet<Contact>();

	public Set<Contact> getContactList() {
		return contactList;
	}

	public void setContactList(Set<Contact> contactList) {
		this.contactList = contactList;
	}

	public void addContact(Contact contact) {
		contactList.add(contact);
	}

	public void delete(Contact contact) {
		contactList.remove(contact);
	}

	public Contact updateContact(int id, Contact contact) {
		for (Contact ct : contactList) {
			if (ct.getId() == id) {
				ct.setAddress(contact.getAddress());
				ct.setName(contact.getName());
				ct.setPhoneNum(contact.getPhoneNum());
			}
		}
		return contact;
	}

	public void search(int id) {
		boolean found = false;
		for (Contact ct : contactList) {
			if (ct.getId() == id) {
				found = true;
				System.out.println(ct.toString());
			}
		}

		if (!found) {
			System.out.println("Not Found");
		}
	}

	public void print() {
		for (Contact contact : contactList) {
			System.out.println(contact);
		}
	}

	@Override
	public String toString() {
		return "PhoneBook [contactList=" + contactList + "]";
	}

}
