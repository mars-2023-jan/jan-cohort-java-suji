package com.mars.training.assignment;

import java.util.Scanner;

public class PhoneBookApp {

	public static void main(String[] args) {

		Contact contact1 = new Contact(1, "David", "Chicago", "973-723-6829");
		Contact contact2 = new Contact(2, "Peter", "Texas", "873-733-8829");
		Contact contact3 = new Contact(3, "Sam", "Georgia", "834-723-9029");
		Contact contact4 = new Contact(4, "Mona", "Chicago", "873-563-6829");
		Contact contact5 = new Contact(5, "Tammy", "Alaska", "864-723-6667");
		Contact contact6 = new Contact(6, "Jenny", "Florida", "890-734-2149");
		Contact contact7 = new Contact(7, "Mark", "Atlanta", "834-423-5229");

		PhoneBook phoneBook = new PhoneBook();
		phoneBook.addContact(contact1);
		phoneBook.addContact(contact2);
		phoneBook.addContact(contact3);
		phoneBook.addContact(contact4);
		phoneBook.addContact(contact5);
		phoneBook.addContact(contact6);
		phoneBook.addContact(contact7);

		phoneBook.print();
		System.out.println("Initial Size: " + phoneBook.getContactList().size());
		System.out.println("--------------");
		phoneBook.delete(contact7);
		phoneBook.print();
		System.out.println("Size after Delete: " + phoneBook.getContactList().size());
		System.out.println("--------------");

		Contact toUpdate = new Contact(1, "David", "Green Bay", "973-723-6829");
		System.out.println(phoneBook.updateContact(1, toUpdate));

		System.out.println("--------------");

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Id to Search :");
		int id = sc.nextInt();
		phoneBook.search(id);

	}

}
