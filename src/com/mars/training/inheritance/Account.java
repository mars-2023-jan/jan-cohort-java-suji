package com.mars.training.inheritance;

public abstract class Account {
	private int accountNumber; // instance variables
	private String accntHolderName;
	private double accountBalance;

	public Account() {
		super();

	}

	public Account(int accountNumber, String accntHolderName, double accountBalance) {
		super();
		this.accountNumber = accountNumber; // initialize instance variable
		this.accntHolderName = accntHolderName;
		this.accountBalance = accountBalance;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccntHolderName() {
		return accntHolderName;
	}

	public void setAccntHolderName(String accntHolderName) {
		this.accntHolderName = accntHolderName;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public void deposit(double amount) {
		this.accountBalance += amount;
	}

	public void withdraw(double amount) throws Exception {
		this.accountBalance -= amount;
	}

//	public abstract double calculate(int roi, int n) throws RuntimeException;
}
