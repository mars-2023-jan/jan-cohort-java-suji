package com.mars.training.inheritance;

import java.util.Scanner;

public class BankDemo {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

//		System.out.println("Enter Account Holder Name: ");
//		String acctHolderName = sc.next();
//
//		System.out.println("Enter Account Balance: ");
//		double acctBalance = sc.nextDouble();
//
//		System.out.println("Enter Minimum Balance : ");
//		float minBalance = sc.nextFloat();

		int randomNum = (int) (Math.random() * (10000000 - 10000) + 10000);
		final int minBalance = 1000;

		SavingsAccount sa1 = new SavingsAccount(randomNum, "Sujitha", 5000, 5, minBalance);
		CheckingAccount ca1 = new CheckingAccount(randomNum, "Sujitha", 5000, minBalance);

//		try {
//			sa1.calculate(8, 2);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		System.out.println("Enter the account type (Savings or Checkings) : ");
		String accountType = sc.nextLine();

		if (accountType.equals("Savings")) {
			System.out.println("Enter 1 for Deposit or 2 for withdraw or 3 for term deposit: ");
			int choice = sc.nextInt();

			if (choice == 1) {
				System.out.println("Enter deposit amount:");
				double amount = sc.nextDouble();
				sa1.deposit(amount);
			} else if (choice == 2) {
				System.out.println("Enter amount to withdraw:");
				double amount = sc.nextDouble();
				try {
					sa1.withdraw(amount);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (choice == 3) {
				System.out.println("Enter Deposit Amount:");
				double amount = sc.nextDouble();
				sa1.setDepositAmount(amount);
				System.out.println("Enter Deposit Duration:");
				sa1.setDurationOfDeposit(sc.nextInt());
				System.out.println("Your estimated maturity amount: " + sa1.getMaturityAmount());
			} else {
				System.out.println("Invalid Entry. Enter 1 or 2 or 3: ");
			}

			System.out.println("Current Balance: " + sa1.getAccountBalance());

		} else if (accountType.equals("Checkings")) {
			System.out.println("Enter 1 for Deposit or 2 for withdraw");
			int choice = sc.nextInt();

			if (choice == 1) {
				System.out.println("Enter deposit amount:");
				double amount = sc.nextDouble();
				sa1.deposit(amount);
			} else if (choice == 2) {
				System.out.println("Enter amount to withdraw:");
				double amount = sc.nextDouble();
				try {
					sa1.withdraw(amount);
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {
				System.out.println("Invalid Entry. Enter 1 or 2: ");
			}

			System.out.println("Current Balance: " + sa1.getAccountBalance());
		} else {
			System.out.println("Invalid Entry: Provide Savings or Checkings only");
		}

	}
}
