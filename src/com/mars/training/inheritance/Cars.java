package com.mars.training.inheritance;

public class Cars extends Vehicle {

	String brand;
	int capacity;

	public Cars() {
		System.out.println("Cars constructor called!");
	}

	public String getVehicleDetails() {
		return super.getVehicleDetails() + " Brand: " + this.brand + " Capacity : " + this.capacity;

	}
}
