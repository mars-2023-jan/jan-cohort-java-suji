package com.mars.training.inheritance;

public class CheckingAccount extends Account {
	private float minBalance;

	public CheckingAccount() {
		super();
	}

	public CheckingAccount(int accountNumber, String accntHolderName, double accountBalance, float minBalance) {
		super(accountNumber, accntHolderName, accountBalance);
		this.minBalance = minBalance;
	}

	public float getMinBalance() {
		return minBalance;
	}

	public void setMinBalance(float minBalance) {
		this.minBalance = minBalance;
	}

	@Override
	public String toString() {
		return "CheckingAccount [minBalance=" + minBalance + ", getAccountNumber()=" + getAccountNumber()
				+ ", getAccntHolderName()=" + getAccntHolderName() + ", getAccountBalance()=" + getAccountBalance()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

	public void withdraw(double amount) throws Exception {
		super.withdraw(amount);
		if (super.getAccountBalance() < minBalance) {
			System.out.println("Warning: Your account balance is less than min balance");
		}
	}

//	@Override
//	public double calculate(int roi, int n) throws RuntimeException {
//		// TODO Auto-generated method stub
//		return 0;
//	}

}
