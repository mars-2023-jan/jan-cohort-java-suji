package com.mars.training.inheritance;

public class SavingsAccount extends Account {

	private float roi;
	private float minBalance;
	private double depositAmount;
	private int durationOfDeposit;

	public SavingsAccount() {
		super();
	}

	public SavingsAccount(int accountNumber, String accntHolderName, double accountBalance, float roi,
			float minBalance) {
		super(accountNumber, accntHolderName, accountBalance);
		this.roi = roi;
		this.minBalance = minBalance;
	}

	public float getRoi() {
		return roi;
	}

	public void setRoi(float roi) {
		this.roi = roi;
	}

	public float getMinBalance() {
		return minBalance;
	}

	public void setMinBalance(float minBalance) {
		this.minBalance = minBalance;
	}

	public double getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(double amount) {
		this.depositAmount = amount;
	}

	public int getDurationOfDeposit() {
		return durationOfDeposit;
	}

	public void setDurationOfDeposit(int durationOfDeposit) {
		this.durationOfDeposit = durationOfDeposit;
	}

	@Override
	public String toString() {
		return "Account No : " + String.valueOf(getAccountNumber()) + "-AccountHolderName : " + getAccntHolderName()
				+ "-Account Balance : " + String.valueOf(getAccountBalance()) + "-MinimumBalance: "
				+ String.valueOf(getMinBalance()) + "-Rate Of Interest : " + String.valueOf(getRoi());
	}

	public void withdraw(double amount) throws Exception {
		if (super.getAccountBalance() < amount) {
			throw new Exception("Amount Overdraft");
		}

		if (super.getAccountBalance() < minBalance) {
			System.out.println("Warning: Your account balance is less than min balance");
		}
		super.withdraw(amount);

	}

	public double getMaturityAmount() {
		return depositAmount + (depositAmount * roi * durationOfDeposit / 100);
	}

//	@override
//	public double calculate(int roi, int n) throws Exception {
//		return (15 * roi) / n;
//	}
}
