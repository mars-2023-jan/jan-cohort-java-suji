package com.mars.training.inheritance;

public class Vehicle {

	String vehicleType;
	String make;
	String color;
	boolean isRegistered;

	public Vehicle() {

		System.out.println("Vehicle constructor called!");
	}

	public String getVehicleDetails() {
		return "[Vehicle : " + this.vehicleType + " Color: " + this.color + " Make : " + this.make + " IsRegistered: "
				+ this.isRegistered;
	}

}
